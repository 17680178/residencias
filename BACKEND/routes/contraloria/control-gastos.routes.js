const express = require("express");
const router = express.Router();

const controlador = require("./../../controllers/contraloria/gastos.controller");
router.post("/registrar-gastos", controlador.registrar);
router.get( "/listar-gastos", controlador.listar);
router.put("/editar-gastos/:id", controlador.editar);
router.delete("/eliminar-gasto/:id", controlador.eliminar);
module.exports = router;