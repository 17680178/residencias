const express = require('express');
const router = express.Router();

const operaciones = require('./../../controllers/enfermera/pacientes-cargo/paciente-cargo.controller');

router.get('/listarPacienteEnfermeraCedula/:enfermeraCargo', operaciones.listarPacienteEnfermera);
router.post('/registrar-paciente-Enfermera', operaciones.registrarPacienteCargo);
router.delete('/eliminar-pacienteCargoEnfermera/:id', operaciones.borrarPacienteCargo);

module.exports = router;