const express = require('express');
const router = express.Router();

const operaciones = require('./../../controllers/enfermera/pacientes-baño/baño-pacientes.controller');

router.get('/listar-baño-pacienteIdCama/:idCama');
router.get('/listar-baño-pacienteEnfermera/:enfermera');
router.post('/registrar-baño-paciente', operaciones.registrarBaño);

module.exports = router;