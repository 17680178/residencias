const express = require("express");
const router = express.Router();

const controlador = require("./../../controllers/farmacia/medicamentos.controller");

//Registrar medicamentos
router.post("/registrar-medicamentos", controlador.registrarMedicamentos);
//listar medicamentos
router.get("/listar-medicamentos", controlador.listarMedicamentos);
//Editar medicamentos
router.put("/editar-medicamentos/:id", controlador.EditarMedicamentos);
//Eliminar medicamentos
router.delete("/eliminar-medicamentos/:id", controlador.EliminarMedicamentos);
module.exports = router;