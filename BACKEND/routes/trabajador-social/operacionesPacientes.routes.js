const express = require("express");
const router = express.Router();

const controlador = require("./../../controllers/trabajo-social/operacionesPacientes.controller");

router.post('/registrarPacienteTrabajoSocial', controlador.RegistrarPacientes);
router.get('/listarPacientesTrabajoSocial', controlador.ListarPacienteGeneral);
router.get('/listarPacienteDoctorCargoTrabajoSocial/:cedula', controlador.ListarDoctorEncargado );
router.delete('/eliminarPacienteTrabajadorSocial/:id', controlador.eliminarPacienteTrabajadorSocial);
module.exports = router;