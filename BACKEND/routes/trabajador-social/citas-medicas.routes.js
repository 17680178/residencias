const express = require("express");
const router = express.Router();

const operaciones = require('./../../controllers/trabajo-social/citas-medicas.controller');

router.post('/registrar-cita', operaciones.registrar);
router.get('/listar-citas', operaciones.listarCitas);
router.get('/listar-citas-fecha/:fecha', operaciones.listarCitasFecha);
router.get('/listar-citas-doctor/:doctor', operaciones.listarCitasDoctor);
router.get('/listar-citar-nombre/:nombre', operaciones.listarCitasNombre);

module.exports = router;