const express = require('express');
const router = express.Router();

const controlador = require('./../../controllers/trabajo-social/control-camas.controller');

router.post('/registrar-cama', controlador.registrarCama);
router.get('/listar-camas', controlador.listarCamas);
router.get('/listar-camaStatus/:disponible');

module.exports = router;