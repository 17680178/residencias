const express = require("express");
const router = express.Router();

const controlador = require('./../../controllers/mobiliario/mobiliario.controller');
router.post('/registrar-mobiliario', controlador.registrarMobiliario);
router.get('/listar-mobiliario', controlador.listarMobiliario);
router.put('/editar-mobiliario/:id', controlador.editarMobiliario);
router.delete('/eliminar-mobiliario/:id', controlador.eliminarMobiliario);

module.exports = router;