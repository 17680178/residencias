const express = require("express");
const router = express.Router();

const operacion = require("./../../controllers/quimicos/estudios-quimicos.controller")

router.post('/registrar-estudio-quimico', operacion.registrarEstudio);

router.get('/listar-estudios', operacion.listarTodosEstudios);
router.get('/listar-estudios-estatus/:estatus', operacion.listarEstudiosStatus);
router.put('/editar-estudios-estatus/:id', operacion.editarEstatus);

module.exports = router;