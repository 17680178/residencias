//CREACION DE LAS VARIABLES A USAR
const express = require("express");
const router = express.Router();

const operaciones = require("./../../controllers/enfermera/cirugias/cirugias.controller");

router.get('/listar-cirujias', operaciones.listarCirugias);

router.post('/registrar-cirugia-enfermera', operaciones.registrarCirugias);
router.get('/listar-cirugia-enfermera/:enfermera',operaciones.listarcirugiaEnfermera);
router.get('/listar-cirugia-doctor/:doctor',operaciones.listarcirugiaDoctor);

module.exports = router;