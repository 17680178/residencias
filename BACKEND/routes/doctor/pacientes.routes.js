//CREACION DE LAS VARIABLES A USAR
const express = require("express");
const router = express.Router();

//MANDAMOS A TRAER EL ARCHIVO CONTROLADOR

const opeaciones = require("../../controllers/doctor/pacientes.controller");

//CREAMOS LAS RUTAS DE CONSUMO
router.get("/listar-pacientes", opeaciones.listarPaciente);  //LISTAR PACIENTE 

router.get('/listar-paciente-idCama/:idCama', opeaciones.listarPacienteNumeroCama);
router.post("/listar-pacientesDoctorEncargado ", opeaciones.listarPacienteDoctorEncargado);  //LISTAR PACIENTE  CON DOCTOR ENCAGADO
router.get("/listarEnfermeraPacienteCED/:numeroCedula", opeaciones.listarEnfermeraPaciente);
//router.post("/registrar-pacientes", opeaciones.registrarPaciente);  //REGISTRAR PACIENTE 

router.put("/editar-pacientes/:id", opeaciones.editarPaciente);  //EDITAR PACIENTE 
router.put("/editar-pacientes-diagnostico/:id", opeaciones.editarPacienteDiagnostico);  //EDITAR PACIENTE 
router.delete("/eliminar-pacientes/:id", opeaciones.borrarPaciente);  //ELIMINAR PACIENTE 

//PREPARAMOS LAS VARIABLES A EXPORTAR

module.exports = router;
