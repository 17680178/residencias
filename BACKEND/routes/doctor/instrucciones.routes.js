
//CREACION DE LAS VARIABLES A USAR
const express = require("express");
const router = express.Router();



//MANDAMOS A TRAER EL ARCHIVO CONTROLADOR

const opeaciones = require("../../controllers/doctor/instrucciones.controller");

//CREAMOS LAS RUTAS DE CONSUMO
//router.get("/listar-citas", opeaciones.listarCita);  //LISTAR CITAS
//router.post("/registrar-citas", opeaciones.registrarCitas);  //REGISTRAR CITAS
router.put("/editar-citas", opeaciones.editarCita);  //EDITAR CITAS
router.delete("/eliminar-citas", opeaciones.borrarCita);  //ELIMINAR CITAS


//PREPARAMOS LAS VARIABLES A EXPORTAR
module.exports = router;
