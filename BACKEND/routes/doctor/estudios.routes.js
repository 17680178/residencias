//CREACION DE LAS VARIABLES A USAR
const express = require("express");
const router = express.Router();

//MANDAMOS A TRAER EL ARCHIVO CONTROLADOR

const opeaciones = require("../../controllers/doctor/estudios.controller");

//CREAMOS LAS RUTAS DE CONSUMO
router.get("/listar-estudios", opeaciones.listarEstudios);  //LISTAR ESTUDIOS 

router.get("/listar-estudios-idHabitacion/:idHabitacion", opeaciones.listarEstudiosNumeroHabitacion);
router.post("/listar-estudios-DoctorEncargado ", opeaciones.listarEstudiosDoctorEncargado);  //LISTAR ESTUDIOS CON DOCTOR ENCAGADO
router.post("/registrar-estudios", opeaciones.registrarEstudios);  //REGISTRAR ESTUDIOS
router.put("/editar-estudios/:id", opeaciones.editarEstudios);  //EDITAR ESTUDIOS

router.delete("/eliminar-estudios/:id", opeaciones.borrarEstudios);  //ELIMINAR ESTUDIOS 

//PREPARAMOS LAS VARIABLES A EXPORTAR

module.exports = router;
