//CREACION DE LAS VARIABLES A USAR
const express = require("express");
const router = express.Router();

//MANDAMOS A TRAER EL ARCHIVO CONTROLADOR

const opeaciones = require("../../controllers/doctor/cirujias_programadas.controller");

//CREAMOS LAS RUTAS DE CONSUMO
router.get("/listar-cirujias", opeaciones.listarCirujias);  //LISTAR PACIENTE 

router.get('/listar-cirujias-idConsultorio/:idConsultorio', opeaciones.listarCirujiasNumeroSala);
router.post("/listar-cirujiasDoctorEncargado ", opeaciones.listarCirujiasDoctorEncargado);  //LISTAR PACIENTE  CON DOCTOR ENCAGADO
router.post("/registrar-cirujias", opeaciones.registrarCirujias);  //REGISTRAR PACIENTE 

router.put("/editar-cirujias/:id", opeaciones.editarCirujias);  //EDITAR PACIENTE 

router.delete("/eliminar-cirujias/:id", opeaciones.borrarCirujias);  //ELIMINAR PACIENTE 

//PREPARAMOS LAS VARIABLES A EXPORTAR

module.exports = router;
