const express = require('express');
const router = express.Router();

const controlador = require('./../../controllers/login/login.controller');

router.post('/login', controlador.login);
router.post('/registrar-usuarios', controlador.register);


router.get('/rol-listar/:rol', controlador.listarUsuarioRol);
router.get('/listar-user-email/:email', controlador.listarEmail);
router.get('/listar-usuariosID/:id', controlador.ListarID);


//editar usuarios
router.put('/editar-usuarios/:id', controlador.editarUsuarios);
router.delete('/eliminar-usuario/:id', controlador.eliminarUsuarios);

router.get('/', (req,res)=>{
    res.json(['Este es un servidor de uso exclusivo para la clinica Morales Cortes y Asociados']);
});

module.exports = router;
