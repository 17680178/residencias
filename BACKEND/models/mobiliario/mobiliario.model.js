const mongoose = require("mongoose");
let mobiliarioSchema = new mongoose.Schema({
    numero_serie:{
        type:String,
        required:true
    },
    nombre_mobiliario:{
        type:String,
        required:true
    },
    seccion_encuentra:{
        type:String,
        required:true
    },
    estatus:{
        type:String,
        required:true
    }
});
module.exports = mongoose.model("mobiliario", mobiliarioSchema);