const mongoose = require("mongoose");

let AbonoSchema = new mongoose.Schema({
    fecha:{
        type:String,
        required:true
    },
    hora:{
        type:String,
        required:true
    },
    monto:{
        required:true,
        type:String
    },
    motivo:{
        type:String,
        required:true
    }
});

module.exports = mongoose.model("abono-caja", AbonoSchema);