const mongoose = require("mongoose");

let GastosSchema = new mongoose.Schema({
    fecha:{
        type:String,
        required:true
    },
    hora:{
        type:String,
        required:true
    },
    monto:{
        required:true,
        type:String
    },
    motivo:{
        type:String,
        required:true
    }
});

module.exports = mongoose.model("control-gastos", GastosSchema);