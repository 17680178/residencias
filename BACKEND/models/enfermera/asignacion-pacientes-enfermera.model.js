const mongoose = require('mongoose');

let asignacionPacientesEnfermeraSchema = new mongoose.Schema({ 
    
    idCama:{
        type:Number,
        required: true,
        unique:true
    },
    enfermeraCargo:{
        type:String,
        required:true
    }

 });

module.exports = mongoose.model('AsignacionPacientesEnfermera', asignacionPacientesEnfermeraSchema);