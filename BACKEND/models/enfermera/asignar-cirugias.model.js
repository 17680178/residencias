const mongoose  = require("mongoose");

let asignar_cirugia = new mongoose.Schema({
    nombre_paciente:{
        type:String,
        required:false
    },
    tipo_cirugia:{
        type:String,
        required:true
    },
    doctor_opera:{
        type:String,
        required:true
    },
    enfermera_apoyo:{
        type:String,
        required:true
    },
    numero_quirofano:{
        type:String,
        required:true
    },
    fecha_operacion:{
        type:String,
        required:true
    },
    hora:{
        type:String,
        required:true
    }
});

module.exports = mongoose.model('cirugias', asignar_cirugia);