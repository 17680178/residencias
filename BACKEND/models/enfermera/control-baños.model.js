const mongoose = require('mongoose');

let registrarBañoSchema = new mongoose.Schema({
    idCama:{
        type:String,
        required:true
    },
    enfermermaCargo:{
        type:String,
        required:true
    },
    fecha_baño:{
        type:String,
        required:false,
    },
    StatusBaño:{
        type:String,
        required:true
    }
});

module.exports = mongoose.model("registrosBaños", registrarBañoSchema);