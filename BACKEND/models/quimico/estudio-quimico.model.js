const mongoose = require('mongoose');

let solicitarEstudiosQuimicos = new mongoose.Schema({
    
    nombre_paciente:{
        type:String,
        required:false
    },
    numero_habitacion:{
        type:String,
        required:false
    },
    estudio_solicitado:{
        type:String,
        required:false
    },
    fecha:{
        type:String,
        required:false
    },
    hora:{
        type:String,
        required:false
    },
    nombre_doctor:{
        required:false,
        type:String
    },
    nivel_prioridad:{
        required:false,
        type:String
    },
    estatus:{
        required:false,
        type:String,
        default:"Enviado"
    }

});
module.exports = mongoose.model("estudios-quimicos", solicitarEstudiosQuimicos);