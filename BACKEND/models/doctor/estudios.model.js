const mongoose = require("mongoose");

let estudiosSchema = new mongoose.Schema({
    idHabitacion:{
        type:Number,
        required: true
    },
    nombre:{
        type:String,
        required:true
    },
    nombreEstudio:{
        type:String,
        required:true
    },
    fecha:{
        type:String,
        required:true
    },
    hora:{
        type:String,
        required:true
    },
    nombre_doctor:{
        type:String,
        required:false
    },
    nivelPrioridad:{
        type:String,
        required:true
    }
});


module.exports = mongoose.model("estudios", estudiosSchema);