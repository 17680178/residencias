const mongoose = require("mongoose");
var pacientesSchema = new mongoose.Schema({
    idCama:{
        type:Number,
        required: false
    },
    nombre:{
        type:String,
        required:false
    },
    apellido_paterno:{
        type:String,
        required:false
    },
    apellido_materno:{
        type:String,
        required:false
    },
    peso:{
        type:String,
        required:false
    },
    edad:{
        type:Number,
        required:false
    },
    diagnostico:{
        type:String,
        required:false
    },
    tipoSangre:{
        type:String,
        required:false
    },
    doctorEncargado:{
        type:String,
        required:false
    },
    enfermeraCargo:{
        type:String,
        required:false
    }
});
module.exports = mongoose.model("pacientes", pacientesSchema);