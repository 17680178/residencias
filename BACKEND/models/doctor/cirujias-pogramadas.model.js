const mongoose = require("mongoose");

let cirujiasProgramadasSchema = new mongoose.Schema({
    idConsultorio:{
        type:Number,
        required: false
    },
    nombre:{
        type:String,
        required:false
    },
    tipo_cirugia:{
        type:String,
        required:false
    },
    fecha:{
        type:String,
        required:false
    },
    hora:{
        type:String,
        required:false
    },
    nombre_doctor:{
        type:String,
        required:false
    }
});


module.exports = mongoose.model("cirujiasProgramadas", cirujiasProgramadasSchema);