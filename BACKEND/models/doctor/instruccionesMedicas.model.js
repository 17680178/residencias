const mongoose = require("mongoose");

let instruccionesMedicasSchema = new mongoose.Schema({
    idCama:{
        type:Number,
        required: true
    },
    nombre:{
        type:String,
        required:true
    },
    apellidoPaterno:{
        type:String,
        required:true
    },
    apellidoMaterno:{
        type:String,
        required:true
    },
    peso:{
        type:String,
        required:true
    },
    tipoSangre:{
        type:String,
        required:true
    },
    edad:{
        type:String,
        required:true
    },
    diagnostico:{
        type:String,
        required:true
    }

});

module.exports = mongoose.model("instruccionesMedicas", instruccionesMedicasSchema);