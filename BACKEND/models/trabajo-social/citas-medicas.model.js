const mongoose = require('mongoose');
var CitasMedicasSchema = new mongoose.Schema({
    nombre_solicitante:{
        required:false,
        type:String
    },
    fecha:{
        type:String,
        required:false
    },
    hora:{
        type:String,
        required:false
    },
    asunto:{
        required:false,
        type:String
    },
    nombre_doctor:{
        type:String,
        required:false
    },
    numero_consultorio:{
        required:false,
        type:String
    },
    estatus:{
        required:false,
        type:String,
        default:'Agendada'
    }
});
module.exports = mongoose.model('citas-medicas', CitasMedicasSchema);