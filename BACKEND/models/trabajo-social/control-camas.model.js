const mongoose = require('mongoose');

let controlCamaSchema = new mongoose.Schema({
    numero_cama:{
        type:String,
        required:true
    },
    status_cama:{
        type:String,
        required:true,
        default:'Disponible'
    },
    idPaciente:{
        required:false,
        type:String,
        default:'Sin paciente registrado'
    }
});

module.exports = mongoose.model('control-camas', controlCamaSchema);