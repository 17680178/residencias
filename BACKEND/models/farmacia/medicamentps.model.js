const mongoose = require("mongoose");

let medicamentosSchema = new mongoose.Schema({
    numero_serie:{
        type:String,
        required:true
    },
    nombre_medicamento:{
        type:String,
        required:true
    },
    tipo_administracion:{
        type:String,
        required:true
    },
    presentacion:{
        type:String,
        required:true
    },
    cantidad:{
        type:String,
        required:true
    },
    vencimineto:{
        required:true,
        type:String
    }
});

module.exports = mongoose.model( "medicamentos-farmacia", medicamentosSchema );