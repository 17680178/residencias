const modeloDatos = require("../../models/doctor/cirujias-pogramadas.model");


//FUNCION PARA BUSCAR TODAS LAS CIRUJIAS PROGRAMADAS 
module.exports.listarCirujias = async (req, res) =>{
    let modelo = await modeloDatos.find();
    if (!modelo){
        res.status(422).json(["No se encuentra el registro"])
    }

    res.json(modelo);
} 

//FUNCION PARA BUSCAR LAS CIRUJIAS PROGRAMADA SEGUN EL DOCTOR ENCARGADO
module.exports.listarCirujiasDoctorEncargado = async (req, res) =>{
    let modelo = await modeloDatos.find({nombre_doctor: req.body.nombre_doctor});
    if (!modelo){
        res.status(422).json(["No se encuentra el registro de la cirujia"])
    }

    res.json(modelo);
} 

//Funcion para listar Cirujias según su número de sala
module.exports.listarCirujiasNumeroSala = async (req,res)=>{
    let modelo = await modeloDatos.find({idConsultorio: req.params.idConsultorio});
    if (!modelo){
        res.status(422).json(["No se encuentra el registro de la cirujia"])
    }

    res.json(modelo);

}

//METODO PARA REGISTRAR LAS CIRUJIAS
module.exports.registrarCirujias =  (req, res) =>{
    let modelo = new modeloDatos(req.body);
    
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡La Cirujia ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
} 


//METODO PARA BORRRAR LAS CIRUJIAS

module.exports.borrarCirujias = async (req, res) =>{
    let modelo = await modeloDatos.findById(req.params.id);
    if (!modelo){
        res.status(422).json(["No se encuentra el registro del Paciente"])
    }

    await modeloDatos.findByIdAndRemove(req.params.id);
    res.send("Los datos han sido eliminados");
} 



//EDITAR LAS CIRUJIAS 
module.exports.editarCirujias = async (req, res) =>{
    
    const {nombre, tipo_cirugia, fecha, hora, nombre_doctor } = req.body;
    let modelo = await modeloDatos.findById(req.params.id);

    if (!modelo){
        res.status(422).json(["No se encuentra el registro"])
    }

    modelo.nombre = nombre;
    /* modelo.apellidoPaterno = apellidoPaterno;
    modelo.apellidoMaterno = apellidoMaterno; */
    modelo.tipo_ciugia = tipo_cirugia;
    modelo.fecha = fecha;
    modelo.hora = hora;
    modelo.nombre_doctor = nombre_doctor;  

    await modeloDatos.findByIdAndUpdate({_id:req.params.id}, modelo, {new:true});
     
    res.json(modelo);
    
}
