const modeloDatos = require("../../models/doctor/instruccionesMedicas.model");


//FUNCION PARA BUSCAR LAS CITA SEGUN EL PARAMETRO
module.exports.listarCita = async (req, res) =>{
    let modelo = await modeloDatos.find();
    if (!modelo){
        res.status(422).json(["No se encuentra la cita"])
    }

    res.json(modelo);
} 

//METODO PARA REGISTRAR CITAS
module.exports.registrarCitas =  (req, res) =>{
    let modelo = new modeloDatos(req.body);
    
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡La cita ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
} 

//METODO PARA BORRRAR CITA

module.exports.borrarCita = async (req, res) =>{
    let modelo = await modeloDatos.findById(req.params.id);
    if (!modelo){
        res.status(422).json(["No se encuentra la cita"])
    }

    await modeloDatos.findByIdAndRemove(req.params.id);
    res.send("El datos han sido eliminados");
} 


//EDITAR CITAS

module.exports.editarCita = async (req, res) =>{
    
    const {nombre, apellidoPaterno, apellidoMaterno, peso, tipoSangre, edad, diagnostico } = req.body;
    let modelo = await modeloDatos.findById(req.params.id);

    if (!modelo){
        res.status(422).json(["No se encuentra la cita"])
    }

    modelo.nombre = nombre;
    modelo.apellidoPaterno = apellidoPaterno;
    modelo.apellidoMaterno = apellidoMaterno;
    modelo.peso = peso;
    modelo.tipoSangre = tipoSangre;
    modelo.edad = edad;
    modelo.diagnostico = diagnostico;  

    await modeloDatos.findByIdAndUpdate({_id:req.params.id}, modelo, {new:true});
     
    res.json(modelo);
    
}
