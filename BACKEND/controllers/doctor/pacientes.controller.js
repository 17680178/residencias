 const modeloDatos = require("../../models/doctor/pacientes.model");
 const modeloUsuario = require("./../../models/login/usuarios.model");

//FUNCION PARA BUSCAR todos los PACIENTES 
module.exports.listarPaciente = async (req, res) =>{
    let modelo = await modeloDatos.find();
    if (!modelo){
        res.status(422).json(["No se encuentra el registro"])
    }

    res.json(modelo);
} 

//FUNCION PARA BUSCAR LOS PACIENTES SEGUN EL DOCTOR ENCARGADO
module.exports.listarPacienteDoctorEncargado = async (req, res) =>{
    let modelo = await modeloDatos.find({doctorEncargado: req.body.idDoctor});
    
    if (!modelo){
        res.status(422).json(["No se encuentra el registro del Paciente"])
    }

    res.json(modelo);
}
module.exports.listarEnfermeraPaciente = async (req,res) =>{
    let nc = req.params.numeroCedula;
    const numero = nc.split('=');
    let modelo = await modeloUsuario.find({numero_cedula:numero[1]});
    
    if(!modelo){
        res.json(["No se encuentra el registro"]);
    }else{
        let nombreCompleto = "";
        nombreCompleto = modelo[0].nombre + " " + modelo[0].apellido_paterno + " " + modelo[0].apellido_materno;
        res.json(nombreCompleto );
    }
} 
//Funcion para listar paciente según su número de cama
module.exports.listarPacienteNumeroCama = async (req,res)=>{
    let modelo = await modeloDatos.find({idCama: req.params.idCama});
    if (!modelo){
        res.status(422).json(["No se encuentra el registro del Paciente"])
    }

    res.json(modelo);

}

//METODO PARA REGISTRAR PACIENTES
module.exports.registrarPaciente =  (req, res) =>{
    let modelo = new modeloDatos(req.body);
    
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡El paciente ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
} 


//METODO PARA BORRRAR PACIENTES

module.exports.borrarPaciente = async (req, res) =>{
    let modelo = await modeloDatos.findById(req.params.id);
    if (!modelo){
        res.status(422).json(["No se encuentra el registro del Paciente"])
    }

    await modeloDatos.findByIdAndRemove(req.params.id);
    res.send("El datos han sido eliminados");
} 



//EDITAR PACIENTES
module.exports.editarPaciente = async (req, res) =>{
    
    const {nombre, apellidoPaterno, apellidoMaterno, peso, edad, diagnostico, tipoSangre } = req.body;
    let modelo = await modeloDatos.findById(req.params.id);

    if (!modelo){
        res.status(422).json(["No se encuentra el registro"])
    }

    modelo.nombre = nombre;
    modelo.apellidoPaterno = apellidoPaterno;
    modelo.apellidoMaterno = apellidoMaterno;
    modelo.peso = peso;
    modelo.edad = edad;
    modelo.diagnostico = diagnostico;
    modelo.tipoSangre = tipoSangre;  

    await modeloDatos.findByIdAndUpdate({_id:req.params.id}, modelo, {new:true});
     
    res.json(modelo);
    
}


module.exports.editarPacienteDiagnostico = async (req, res) =>{
    
    const {diagnostico} = req.body;
    let modelo = await modeloDatos.findById(req.params.id);

    if (!modelo){
        res.status(422).json(["No se encuentra el registro"])
    }

    modelo.diagnostico = diagnostico

    await modeloDatos.findByIdAndUpdate({_id:req.params.id}, modelo, {new:true});
     
    res.json(modelo);
    
}