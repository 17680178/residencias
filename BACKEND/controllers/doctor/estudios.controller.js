const modeloDatos = require("../../models/doctor/estudios.model");


//FUNCION PARA BUSCAR TODOS LOS ESTUDIOS 
module.exports.listarEstudios = async (req, res) =>{
    let modelo = await modeloDatos.find();
    if (!modelo){
        res.status(422).json(["No se encuentra el registro"])
    }

    res.json(modelo);
} 

//FUNCION PARA BUSCAR TODOS LOS ESTUDIOS  SEGUN EL DOCTOR ENCARGADO
module.exports.listarEstudiosDoctorEncargado = async (req, res) =>{
    let modelo = await modeloDatos.find({nombre_doctor: req.body.nombre_doctor});
    if (!modelo){
        res.status(422).json(["No se encuentra el registro del estudio"])
    }

    res.json(modelo);
} 

//Funcion para listar estudios según su número de habitacion
module.exports.listarEstudiosNumeroHabitacion = async (req,res)=>{
    let modelo = await modeloDatos.find({idHabitacion: req.params.idHabitacion});
    if (!modelo){
        res.status(422).json(["No se encuentra el registro del estudio"])
    }

    res.json(modelo);

}

//METODO PARA REGISTRAR LOS ESTUDIOS
module.exports.registrarEstudios =  (req, res) =>{
    let modelo = new modeloDatos(req.body);
    
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡El Estudio ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
} 


//METODO PARA BORRRAR LOS ESTUDIOS

module.exports.borrarEstudios = async (req, res) =>{
    let modelo = await modeloDatos.findById(req.params.id);
    if (!modelo){
        res.status(422).json(["No se encuentra el registro del Estudio"])
    }

    await modeloDatos.findByIdAndRemove(req.params.id);
    res.send("Los datos han sido eliminados");
} 



//EDITAR LOS ESTUDIOS 
module.exports.editarEstudios = async (req, res) =>{
    
    const {nombre, nombreEstudio, fecha, hora, nombre_doctor, nivelPrioidad } = req.body;
    let modelo = await modeloDatos.findById(req.params.id);

    if (!modelo){
        res.status(422).json(["No se encuentra el registro"])
    }

    modelo.nombre = nombre;
    /* modelo.apellidoPaterno = apellidoPaterno;
    modelo.apellidoMaterno = apellidoMaterno; */
    modelo.nombreEstudio = nombreEstudio;
    modelo.fecha = fecha;
    modelo.hora = hora;
    modelo.nombre_doctor = nombre_doctor;  
    modelo.nivelPrioidad = nivelPrioidad;

    await modeloDatos.findByIdAndUpdate({_id:req.params.id}, modelo, {new:true});
     
    res.json(modelo);
    
}
