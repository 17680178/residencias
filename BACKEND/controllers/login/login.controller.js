const modeloCandidatos = require('./../../models/login/usuarios.model');

module.exports.login = async (req,res)=>{
    let modelo = await modeloCandidatos.find({email:req.body.email, password:req.body.password});
    if(!modelo){
        res.status(422).json(["No se encuentra registro"]);
    }else{
        if(modelo == ""){
            res.json(["No existe ningun registro"]);
        }else{
            let rol = modelo[0].rol;
            res.json(rol);
        }
    }
}

module.exports.register = function(req,res){
    let modelo = new modeloCandidatos(req.body);
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡El usuario ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
}


//Busqueda de usuarios rol enfermeraría y otros.

module.exports.listarUsuarioRol = async (req,res)=>{
    let modelo = await modeloCandidatos.find({rol:req.params.rol});
    if(!modelo){
        res.status(422).json(["No se encontro el usuario buscado"]);
    }else{
        res.json(modelo);
    }
}

//Listar usuario por email
module.exports.listarEmail = async (req,res)=>{
    let modelo = await modeloCandidatos.find({email:req.params.email});
    if(!modelo){
        res.status(422).json(["No se encuentra registro"]);
    }else{
        //let rol = modelo[0].rol;
        res.json(modelo);
    }
}
//listar usuarios ID
module.exports.ListarID = async(req,res)=>{
    let modelo = await modeloCandidatos.findById(req.params.id);
    if(!modelo){
        res.status(422).json(["No es posible realizar la busqueda 422 ERROR"]);
    }
    else{
        res.json(modelo);
    }
}
//Editar usuarios
module.exports.editarUsuarios = async (req,res)=>{
    const { 
        email, 
        password, 
        nombre, 
        apellido_paterno,
        apellido_materno,
        numero_cedula,
        rol,
        celular,
        sexo
    } = req.body;
    let modelo = await modeloCandidatos.findById(req.params.id);
    if(!modelo){
        res.status(422).json(["No se encuntra registro del alumno seleccionado"]);
    }
    else{
        modelo.email = email;
        modelo.password = password;
        modelo.nombre = nombre;
        modelo.apellido_paterno = apellido_paterno;
        modelo.apellido_materno = apellido_materno;
        modelo.numero_cedula = numero_cedula;
        modelo.rol = rol;
        modelo.celular = celular;
        modelo.sexo = sexo;
        
        await modeloCandidatos.findByIdAndUpdate({_id:req.params.id}, modelo, {new:true});
        res.json(["Actualización completa."]);
    }
}

module.exports.eliminarUsuarios = async (req,res)=>{
    let modelo = await modeloCandidatos.findById(req.params.id);
    if(!modelo){
        res.status.json(["Sin registro"]);
    }else{
        await modeloCandidatos.findByIdAndRemove(req.params.id);
        res.json("Registro eliminado exitosamente!");
    }
}