const modeloMedicamentos = require("./../../models/farmacia/medicamentps.model");

//Registrar medicamentos
module.exports.registrarMedicamentos = (req,res)=>{
    let modelo = new modeloMedicamentos(req.body);
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡La cita ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
}
//Listar medicamentos farmacia.
module.exports.listarMedicamentos = async(req,res)=>{
    let modelo = await modeloMedicamentos.find();
    if(!modelo){
        res.status(422).json(["No se encontraron medicamentos"]);
    }else{
        res.json(modelo);
    }
};

//Editar medicamentos.
module.exports.EditarMedicamentos = async (req,res) => {
const { numero_serie, nombre_medicamento, tipo_administracion, presentacion, cantidad,vencimineto} = req.body;
    let modelo = await modeloMedicamentos.findById(req.params.id);
    if(!modelo){
        res.status(422).json(["No se encuentra registro"]);
    }else{
        modelo.numero_serie = numero_serie;
        modelo.nombre_medicamento = nombre_medicamento;
        modelo.tipo_administracion = tipo_administracion;
        modelo.presentacion = presentacion;
        modelo.cantidad = cantidad;
        modelo.vencimineto = vencimineto;

        await modeloMedicamentos.findByIdAndUpdate({_id:req.params.id}, modelo, {new:true});
        res.json(["Datos Actualizados correctamente"]);
    }

}

//Eliminar medicamentos

module.exports.EliminarMedicamentos = async(req,res)=>{
    let modelo = await modeloMedicamentos.findById(req.params.id);
    if(!modelo){
        res.status(422).json(["No se encontro registro"]);
    }else{
        await modeloMedicamentos.findByIdAndRemove({_id:req.params.id});
    }
}