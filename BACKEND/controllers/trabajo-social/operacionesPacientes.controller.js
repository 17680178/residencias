const modeloRegistroPacientes = require("./../../models/doctor/pacientes.model"); 
///////////////////////////////////////////////////////
/////////////Registrar pacientes//////////////////////
/////////////////////////////////////////////////////
module.exports.RegistrarPacientes = (req,res)=>{
    let modelo = new modeloRegistroPacientes(req.body);
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡El usuario ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
}
/////////////////////////////////////////////////////
/////////////Listar  pacientes//////////////////////
///////////////////////////////////////////////////
module.exports.ListarPacienteGeneral = async function(req,res){
    let modelo = await modeloRegistroPacientes.find();
    if(!modelo){
        res.status(422).json(["No es posible realizar la busqueda 422 ERROR"]);
    }
    else{
        res.json(modelo);
    }
}

module.exports.ListarDoctorEncargado = async function( req,res ){
    let modelo = await modeloRegistroPacientes.find({doctorEncargado:req.params.cedula});
    if(!modelo){
        res.status(422).json(["No es posible realizar la busqueda 422 ERROR"]);
    }
    else{
        res.json(modelo);
    }
}

/////////////////////////////////////////////////////////////////
//////////////////eliminar pacientes////////////////////////////
///////////////////////////////////////////////////////////////
module.exports.eliminarPacienteTrabajadorSocial = async function(req,res){
    let modelo = await modeloRegistroPacientes.findById(req.params.id);
    
    if(!modelo){
        res.status.json(["Sin registro"]);
    }else{
        await modeloRegistroPacientes.findByIdAndRemove(req.params.id);
        res.json("Registro eliminado exitosamente!");
    }
}