const modeloCitas = require('./../../models/trabajo-social/citas-medicas.model');

module.exports.registrar = (req,res)=>{
    let modelo = new modeloCitas(req.body);
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡El paciente ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
}

module.exports.listarCitas = async (req,res)=>{
    let modelo = await modeloCitas.find();
    if(!modelo){
        res.status(422).json(["No se encuentra datos que coincidan"]);
    }else{
        res.json(modelo);
    }
}

module.exports.listarCitasFecha = async (req,res)=>{
    let modelo = await modeloCitas.find({fecha:req.params.fecha});
    if(!modelo){
        res.status(422).json(["No se encuentra datos que coincidan"]);
    }else{
        res.json(modelo);
    }
}

module.exports.listarCitasNombre = async (req,res)=>{
    let modelo = await modeloCitas.find({nombre_solicitante:req.params.nombre_solicitante});
    if(!modelo){
        res.status(422).json(["No se encuentra datos que coincidan"]);
    }else{
        res.json(modelo);
    }
}

module.exports.listarCitasDoctor = async (req,res)=>{
    let modelo = await modeloCitas.find({nombre_doctor:req.params.nombre_doctor });
    if(!modelo){
        res.status(422).json(["No se encuentra datos que coincidan"]);
    }else{
        res.json(modelo);
    }
}