const modeloCamas = require('./../../models/trabajo-social/control-camas.model');

module.exports.registrarCama = (req,res)=>{
    let modelo = new modeloCamas(req.body);
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡El paciente ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
}

//Listar camas en general
module.exports.listarCamas = async (req,res)=>{
    let modelo = await modeloCamas.find();
    if(!modelo){
        res.status(422).json(["No se encontro registro alguno"]);
    }else{
        res.json(modelo);
    }
}
//Listar camas según su status.
module.exports.listarCamaStatus = async (req,res)=>{
    let modelo = await modeloCamas.find({status_cama:req.params.disponible});
    if(!modelo){
        res.status(422).json(["No se encontro registro"]);
    }else{
        res.json(modelo);
    }
}