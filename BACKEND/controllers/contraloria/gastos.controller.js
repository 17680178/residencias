const modeloGastos = require("./../../models/contraloria/gastos.model");

module.exports.registrar = function(req,res){
    let modelo = new modeloGastos(req.body);
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡La cita ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
}
module.exports.listar = async(req,res)=>{
    let modelo = await modeloGastos.find();
    if(!modelo){
        res.status(422).json(["No se encuentran registros"]);
    }else{
        res.json(modelo)
    }

}
module.exports.editar = async(req,res)=>{
    const { fecha,hora,monto,motivo } = req.body;
    let modelo = await modeloGastos.findById(req.params.id);
    if(!modelo){
        res.status(422).json(["No se encuentran registros"]);
    }   else{
        modelo.fecha = fecha;
        modelo.hora = hora;
        modelo.monto = monto;
        modelo.motivo = motivo;
        await modeloGastos.findByIdAndUpdate({_id:req.params.id},modelo, {new:true});
        res.json(["Actualización completa"]);
    }
}
module.exports.eliminar = async(req,res)=>{
    let modelo = await modeloGastos.findById(req.params.id);
    if(!modelo){
        res.status(422).json(["No se encuentran registros"]);
    }else{
        await modeloGastos.findByIdAndRemove({_id:req.params.id});
        res.json(["Registro eliminado satisfactoriamente"]);
    }
}

/* ===================================================== */
/*                  C O N T R O L       D E         A B O N O S                             */
/* ==================================================== */
const modeloAbono = require("./../../models/contraloria/abono-caja.model");

//Registrar abono
module.exports.registrarAbono = (req,res)=>{
    let modelo = new modeloAbono(req.body);
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡La cita ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
}
//Listar abonos a cajas
module.exports.ListarAbonos = async(req,res)=>{
    let modelo = await modeloAbono.find();
    if(!modelo){
        res.status(422).json(["No se pudo realizar la consulta"]);
    }else{
        res.json(modelo);
    }
}
//Listar vía ID
module.exports.ListarAbonosID = async (req,res)=>{
    let modelo = await modeloAbono.findById(req.params.id )
    if(!modelo){
        res.status(422).json(["No se pudo realizar la consulta"]);
    }else{
        res.json(modelo);
    }
}
//Editar Abono
module.exports.EditarAbonos = async (req,res)=>{
    const { fecha,hora,monto,motivo } = req.body;
    let modelo = await modeloAbono.findById(req.params.id )
    if(!modelo){
        res.status(422).json(["No se pudo realizar la consulta"]);
    }else{
        modelo.fecha = fecha;
        modelo.hora = hora;
        modelo.monto = monto;
        modelo.motivo = motivo;

        await modeloAbono.findByIdAndUpdate({_id:req.params.id}, modelo, {new:true} );
        res.json(["Campos actualizados correctamente"]);
    }
}
//Eliminar Abono
module.exports.EliminarAbonos = async (req,res)=>{
    let modelo = await modeloAbono.findById(req.params.id )
    if(!modelo){
        res.status(422).json(["No se pudo realizar la consulta"]);
    }
    else{
        await modeloAbono.findByIdAndRemove({_id:req.params.id});
    }
}