const modeloMobiliario = require("./../../models/mobiliario/mobiliario.model");

//Registrar mobiliario
module.exports.registrarMobiliario = (req,res)=>{
    let modelo = new modeloMobiliario(req.body);
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡La cita ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
}
//Listar Mobiliario
module.exports.listarMobiliario = async(req,res)=>{
    let modelo = await modeloMobiliario.find();
    if(!modelo){
        res.status(422).json(["No se encontro registro"]);
    }else{
        res.json(modelo);
    }
}
//Editar mobiliario
module.exports.editarMobiliario = async (req,res)=>{
    const { 
        numero_serie,
        nombre_mobiliario,
        seccion_encuentra,
        estatus
     } = req.body;
    let modelo = await modeloMobiliario.findById(req.params.id);
    if(!modelo){
        res.status(422).json(["No se encontro registro"]);
    }else{
        modelo.numero_serie = numero_serie;
        modelo.nombre_mobiliario = nombre_mobiliario;
        modelo.seccion_encuentra = seccion_encuentra;
        modelo.estatus = estatus;

        await modeloMobiliario.findByIdAndUpdate({_id:req.params.id}, modelo, { new:true } );
        res.json(["Datos actualizados correctamente"]);

    }
}
//Elimiar mobiliario
module.exports.eliminarMobiliario = async(req,res)=>{
    let modelo = await modeloMobiliario.findById(req.params.id);
    if(!modelo){
        res.status(422).json(["No se encontro registro"]);
    }else{
        await modeloMobiliario.findByIdAndRemove(req.params.id);
    }
};