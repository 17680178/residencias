const modeloEstudios = require("./../../models/quimico/estudio-quimico.model");

module.exports.listarTodosEstudios = async (req,res)=>{
    let modelo = await modeloEstudios.find();
    if(modelo){
        res.json(modelo);
    }
    else{
        res.status.json(["No se encontro modelo"]);
    }
}

module.exports.listarEstudiosStatus = async (req,res)=>{
    let modelo = await modeloEstudios.find({estatus:req.params.estatus});
    if(!modelo){
        res.status(422).json(["No se encontro registro alguno"]);
    }else{
        res.json(modelo);
    }
}

//
module.exports.registrarEstudio = (req,res)=>{
    let modelo = new modeloEstudios(req.body);
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡La cita ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
}

module.exports.editarEstatus = async(req,res)=>{
    const {estatus} = req.body;
    let modelo = await modeloEstudios.findById(req.params.id);
    if(!modelo){
        res.status.json(["No se encuentra registro"]);
    }
    else{
        modelo.estatus = estatus;
        await modeloEstudios.findByIdAndUpdate({_id:req.params.id}, modelo, {new:true});
        res.json(["Dato actualizado correctamente"]);
    }
}


