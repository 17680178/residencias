const res = require('express/lib/response');
const modeloPaciente = require('./../../../models/enfermera/asignacion-pacientes-enfermera.model');

// Registrar paciente enfermera
module.exports.registrarPacienteCargo = (req,res)=>{
    let modelo = new modeloPaciente(req.body);
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡La cita ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
}

module.exports.listarEnfermeras = ( req, res ) =>{
    
}

//Listar paciente cargo
module.exports.listarPacienteEnfermera = async (req,res) =>{
    let modelo = await modeloPaciente.find({enfermeraCargo:req.params.enfermeraCargo});
    if(!modelo){
        res.status(422).json(["No se encuentra paciente"]);
    }
}

//borrar paciente
module.exports.borrarPacienteCargo = async ()=>{
    let modelo = await modeloPaciente.findById(req.params.id);
    if(!modelo){
        res.status(422).json(["Sin datos aquí. :( "]);
    }else{
        await modeloPaciente.findByIdAndDelete(req.params.id);
        res.json(["Registro borrado exitosamente."])
    }
}