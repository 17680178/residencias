const modeloCirugias = require('./../../../models/enfermera/asignar-cirugias.model');

//Registrar cirugia 
module.exports.registrarCirugias = (req,res)=>{
    let modelo = new modeloCirugias(req.body);
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡La cirugia ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
}
//Listar cirugias

module.exports.listarCirugias = async(req,res)=>{
    let modelo = await modeloCirugias.find();
    if(modelo){
        res.json(modelo);
    }
}

//Listar enfermera cirugia
module.exports.listarcirugiaEnfermera = async(req,res)=>{
    let modelo = await modeloCirugias.find({enfermera_apoyo:req.params.enfermera_apoyo});
    if(modelo){
        res.json(modelo);
    }
}

//Listar doctor cirugia
module.exports.listarcirugiaDoctor = async(req,res)=>{
    let modelo = await modeloCirugias.find({doctor_opera:req.params.doctor_opera});
    if(modelo){
        res.json(modelo);
    }
}

//Pendiente editar y rutas