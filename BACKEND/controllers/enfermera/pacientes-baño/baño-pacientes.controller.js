const modeloPacientes = require('./../../../models/enfermera/control-baños.model');

//Registrar baños
module.exports.registrarBaño = (req,res)=>{
    let modelo = new modeloPacientes(req.body);
    modelo.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }else{
            if(err.code == 11000){
                res.status(422).send(["¡El baño ya existe!"]);
            }else{
                return next(err);
            }
        }
    });
}
//listar baños

module.exports.listarBañoXCama = async (req,res)=>{
    let modelo = await modeloPacientes.find({idCama:req.params.idCama});
    if(modelo){
        res.json(modelo);
    }else{
        res.status(422).json(["Sin registro"]);
    }
}

module.exports.listarBañoEnfermeraCargo = async (req,res)=>{
    let modelo = await modeloPacientes.find({enfermermaCargo:req.params.enfermermaCargo});
    if(modelo){
        res.json(modelo);
    }else{
        res.status(422).json(["Sin registro"]);
    }
}