/* ===================================================*/
/* Definimos las variables a utilizar en la clase*/
/* ==================================================*/
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
/* ====================================================*/
/*          Traemos la configuracion para consumir mongo atlas               */
/* ===================================================*/
require('./config/config');
require('./models/db');
/* ===================================================*/
/*                             Configuramos los middlewares                             */
/* ==================================================*/
app.use(bodyParser.json());
app.use(cors());
app.use(passport.initialize());
/* ===================================================*/
/*                                Rutas de controladores                                      */
/* ==================================================*/
const opCitas = require('./routes/doctor/instrucciones.routes');
const opPacientes = require('./routes/doctor/pacientes.routes');
const opLogin = require('./routes/login/login.routes');
const opPacienteCargoEnfermera = require('./routes/enfermera/paciente-cargo.routes');
const opBañosPacientes = require('./routes/enfermera/baños-paciente.routes');
const opCirugiasEnfermera = require('./routes/cirugias/cirugias.routes');
const opCamasControl = require('./routes/trabajador-social/control-camas.routes');
const opCitasMedicas = require('./routes/trabajador-social/citas-medicas.routes');
const opPacientesTrabajadorSocial = require('./routes/trabajador-social/operacionesPacientes.routes'); 
const opQuimicos = require('./routes/quimicos/quimico.routes');
const opCirujias = require('./routes/doctor/cirujias.routes');
const opEstudios = require('./routes/doctor/estudios.routes');
const opMobiliario = require('./routes/mobiliario/mobiliario.routes');
const opFarmacia = require("./routes/farmacia/medicamentos.routes");
const opControlGastos = require("./routes/contraloria/control-gastos.routes");


app.use('/api', opCitas);
app.use('/api', opPacientes);
app.use('/api', opCirujias);
app.use('/api', opEstudios);

/* Rutas jorge */

app.use('/api', opLogin);
app.use('/api', opPacienteCargoEnfermera);
app.use('/api', opBañosPacientes);
app.use('/api', opCirugiasEnfermera);
app.use('/api', opCamasControl);
app.use('/api', opCitasMedicas);
app.use('/api', opQuimicos);
app.use('/api', opMobiliario);
app.use('/api', opFarmacia);
app.use('/api', opControlGastos);
app.use('/api', opPacientesTrabajadorSocial);
/* ===================================================*/
/*         Manejador de errores                      */
/* ==================================================*/
app.use( (err,res) => {
    if(err.name == 'ValidationError'){
        var valErrors = [];
        Object.keys(err.errors).forEach(key => valErrors.push(err.errors[key].message));
        res.status(422).send(valErrors);
    }
} );


/* ===================================================*/
/*         Inicializamos el servidor                      */
/* ==================================================*/
app.listen( process.env.PORT, () => {
    console.log(` Servidor iniciado en el puerto  ${process.env.PORT}` );
});